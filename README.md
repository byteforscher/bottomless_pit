_A bottomless pit for your logging_

Just post an json array to / and have all items json encoded piped to syslog

## Getting started

    bundle
    bundle exec unicorn -c config/unicorn.rb --port 5000

    # Test it
    curl -H "Content-Type: application/json" -d '[{"a":1},{"b":2}]' http://localhost:5000

## Tests

    # Linux only so far
    bundle exec rspec app_spec.rb
