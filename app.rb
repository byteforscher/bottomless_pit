require 'bundler'
Bundler.setup
require 'syslog'
require 'json'
require 'sinatra'
require 'sinatra/base'
require 'sinatra/contrib'

class BottomlessPit < Sinatra::Base

  error do
    'Sorry there was a nasty error - ' + env['sinatra.error'].message
  end

  get /\A\/?\z/ do
    "OK"
  end

  post /\A\/?\z/ do
    # FIXME better use https://rubygems.org/gems/yajl-ruby for streaming parsing
    # FIXME proper exception handling - don't just rescue
    parsed = JSON.parse(request.body.read) rescue []

    Syslog.open('bottomless_pit') do |logger|
      parsed.each { |item| logger.info item.to_json }
    end

    "OK"
  end

  get '/.well-known/status' do
    json({status: {code: 'OK', message: 'OK'}})
  end

  helpers do
  end

end
