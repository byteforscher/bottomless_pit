require 'rubygems'
require 'rspec'
require 'rack/test'
require 'json'
require 'open3'

require_relative 'app'

def app
  BottomlessPit
end

def parsed_response
  get_response
  JSON.parse(last_response.body) rescue nil
end

RSpec.configure do |config|
  config.order = "random"
  config.formatter = :documentation

  config.include Rack::Test::Methods
end

describe 'BottomlessPit' do

  let(:get_response) do
    get uri
    last_response
  end

  subject { get_response }

  describe 'GET /' do
    let(:uri) { '/' }
    specify { expect(subject.status).to eq(200) }
  end

  describe 'POST /' do
    let(:uri) { '/' }
    let(:payload) { [{a: 1}, {b: 2}] }
    let(:json_payload) { payload.to_json }

    subject { post(uri, json_payload, { "CONTENT_TYPE" => "application/json" }) }

    specify { expect(subject.status).to eq(200) }

    it 'should send the proper messages to syslog' do
      # this might fail sometimes if not flushed right away
      Open3.popen2e('timeout 3 tail -n 0 -f /var/log/syslog') {|stdin, stdout_and_stderr, wait_thr|
        pid = wait_thr.pid # pid of the started process.

        subject

        exit_status = wait_thr.value

        s = stdout_and_stderr.read

        payload.each do |item|
          expect( s =~ /bottomless_pit\[\d+\]: #{item.to_json}/  ).to_not be_nil
        end
      }
    end

    context 'invalid payload' do
      let(:json_payload) { '{[123...' }
      specify { expect(subject.status).to eq(200) }
    end

  end

  describe '/.well-known' do
    describe 'GET /status' do
      let(:uri) { "/.well-known/status" }
      specify { expect(subject.status).to eq(200) }
    end
  end

end
